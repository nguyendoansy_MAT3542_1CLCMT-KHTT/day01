<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./style.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <title>Đăng ký tân sinh viên</title>
</head>

<body>
<div class="container">
    <!-- Form đăng ký với phương thức post tới confirm.php để hiển thị thông tin đăng ký -->
    <form id="registrationForm" class="reg-form" method="POST" action="confirm.php" enctype="multipart/form-data">
        <!-- Hiển thị lỗi trong việc điền đơn đăng ký -->
        <div id="errorMessages" class="error-messages">
            <script>
                $(document).ready(function () {
                    $("#register_btn").click(function () {
                        let name = $("#name").val();
                        let gender = $("input[name='gender']:checked").val();
                        let department = $("#department").val();
                        let birthdate = $("#birthdate").val();

                        // Biến flag để check xem có error messages về việc đăng không
                        let flag = true;

                        let errorMessages = [];

                        if (name === "") {
                            errorMessages.push("Hãy nhập tên.");
                            flag = false
                        }

                        if (!gender) {
                            errorMessages.push("Hãy chọn giới tính.");
                            flag = false
                        }

                        if (department === "") {
                            errorMessages.push("Hãy chọn phân khoa.");
                            flag = false
                        }

                        let datePattern = /^\d{2}\/\d{2}\/\d{4}$/;
                        if (birthdate === "" || !datePattern.test(birthdate)) {
                            errorMessages.push("Hãy nhập ngày sinh đúng định dạng dd/mm/yyyy.");
                            flag = false
                        }

                        if (!flag) {
                            let errorMessageHtml = "";
                            for (let i = 0; i < errorMessages.length; i++) {
                                errorMessageHtml += errorMessages[i] + "<br>";
                            }
                            $("#errorMessages").html(errorMessageHtml);
                        } else {
                            // Nếu không có lỗi, thực hiện đăng ký hoặc xử lý form ở đây
                            $("#errorMessages").html(""); // Xóa thông báo lỗi cũ
                        }
                    });
                });
            </script>
        </div>
        <!-- Mục Họ và tên -->
        <div class="form-group">
            <div class="full-name-box required-label"><label for="name">Họ và tên</label></div>
            <input class="full-name-input" type="text" id="name" name="name" required>
        </div>
        <!-- Mục Giới tính -->
        <div class="form-group">
            <div class="gender-box required-label"><label for="gender">Giới tính</label></div>
            <div id="gender" class="gender-select">
                <input type="radio" id="male" name="gender" value="Nam" required><label for="male">Nam</label>
                <input type="radio" id="female" name="gender" value="Nữ" required><label for="female">Nữ</label>
            </div>
        </div>
        <!-- Mục Phân khoa -->
        <div class="form-group">
            <div class="department-box required-label"><label for="department">Phân khoa</label></div>
            <select id="department" name="department" class="department-select">
                <option value="">--Chọn phân khoa--</option>
                <?php
                $departments = array('MAT' => 'Khoa học máy tính'
                                    ,'KDL' => 'Khoa học vật liệu');
                foreach ($departments as $key => $value) {
                    echo "<option value=\"$key\">$value</option>";
                }
                ?>
            </select>
        </div>
        <!-- Mục Ngày sinh -->
        <div class="form-group">
            <div class="birth-box required-label"><label for="birthdate">Ngày sinh</label></div>
            <input class="birth-input" type="text" id="birthdate" name="birthdate" placeholder="dd/mm/yyyy" required>
        </div>
        <!-- Mục địa chỉ -->
        <div class="form-group">
            <div class="address-box"><label for="address"></label>Địa chỉ</div>
            <input class="address-input" type="text" id="address" name="address">
        </div>
        <!-- Mục Hình ảnh -->
        <div class="form-group">
            <div class="image-box"><label for="image"></label>Hình ảnh</div>
            <input class="image-select" type="file" id="image" name="profileImage" accept="image/*">
        </div>
        <!-- Nút Đăng ký -->
        <div class="signup-button" id="register_btn">
            <button type="submit">Đăng ký</button>
        </div>
    </form>
</div>
</body>



</html>