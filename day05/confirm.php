<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./style.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <title>Đăng ký tân sinh viên</title>
</head>

<?php
$departments = array('MAT' => 'Khoa học máy tính'
                    ,'KDL' => 'Khoa học vật liệu');
?>

<body>
    <div class="container">
        <form id="registrationForm" class="reg-form">
            <!-- Hiển thị Họ và tên trong form đăng ký  -->
            <div class="form-group">
                <div class="full-name-box required-label"><label for="name">Họ và tên</label></div>
                <div class="show-name">
                    <?php
                    echo $_POST['name'];
                    ?>
                </div>
            </div>
            <!-- Hiển thị Giới tính trong form đăng ký  -->
            <div class="form-group">
                <div class="gender-box required-label"><label for="gender">Giới tính</label></div>
                <div class="show-gender">
                        <?php
                        echo $_POST['gender'];
                        ?>
                </div>
            </div>
            <!-- Hiển thị Phân khoa trong form đăng ký  -->
            <div class="form-group">
                <div class="department-box required-label"><label for="department">Phân khoa</label></div>
                <div class="show-department">
                    <?php
                    echo $departments[$_POST['department']];
                    ?>
                </div>
            </div>
            <!-- Hiển thị Ngày sinh trong form đăng ký  -->
            <div class="form-group">
                <div class="birth-box required-label"><label for="birthdate">Ngày sinh</label></div>
                <div class="show-birthdate">
                    <?php
                    echo $_POST['birthdate'];
                    ?>
                </div>
            </div>
            <!-- Hiển thị Địa chỉ (nếu có)  -->
            <div class="form-group">
                <div class="address-box"><label for="address"></label>Địa chỉ</div>
                <div class="show-address">
                    <?php
                    echo $_POST['address'];
                    ?>
                </div>
            </div>
            <!-- Hiển thị Hình ảnh (nếu có)  -->
            <div class="form-group" style="align-items: unset">
                <div class="image-box" style="height: 20%"><label for="image"></label>Hình ảnh</div>
                <div class="show-image">
                <?php
                if (isset($_FILES['profileImage'])) {
                    if ($_FILES['profileImage']['error'] == 0) {
                        $target_dir = 'uploads/';
                        // Trong trường hợp chưa có thư mục thì ta tiến hành khởi tạo
                        if (!file_exists($target_dir)) {
                            mkdir($target_dir, 0777, true);
                        }
                        $target_file = $target_dir . basename($_FILES['profileImage']['name']);
                        $uploads = true;
                        $maxFileSize = 1000000; // 1000KB
                        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

                        // Kiểm tra Size của ảnh
                        if ($_FILES['profileImage']['size'] > $maxFileSize) {
                            echo '<div>Kích thước của ảnh quá lớn</div>';
                            $uploads = false;
                        }

                        // Kiểm tra định dạng của ảnh
                        if($imageFileType != "jpg"
                            && $imageFileType != "png"
                            && $imageFileType != "jpeg"
                            && $imageFileType != "gif" ) {
                            echo '<div>Sai định dạng của ảnh</div>';
                            $uploads = false;
                        }

                        if ($uploads) {
                            if (move_uploaded_file($_FILES['profileImage']['tmp_name'], $target_file)) {
                                echo '<img src="./uploads/' . $_FILES["profileImage"]['name'] . '"style="width:100%">';
                            } else {
                                echo 'Không tải lên được ảnh';
                            }
                        }
                    }
                }
                ?>
                </div>
            </div>
            <!-- Nút Xác nhận  -->
            <div class="submit-button">
                <button type="submit">Xác nhận</button>
            </div>
        </form>
    </div>
</body>

</html>