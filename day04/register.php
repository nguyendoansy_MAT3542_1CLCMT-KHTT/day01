<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./style.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <title>Đăng ký tân sinh viên</title>
</head>

<body>
    <div class="container">
        <form id="registration-form" class="bd-blue">
            <!-- Invalid -> Message -->
            <div id="errorMessages" class="error">
                <script>
                    $(document).ready(function () {
                        $("#register-btn").click(function () {
                            var name = $("#name").val();
                            var gender = $("input[name='gender']:checked").val();
                            var department = $("#department").val();
                            var birthdate = $("#birthdate").val();

                            var errorMessages = [];

                            if (name === "") {
                                errorMessages.push("Hãy nhập tên.");
                            }

                            if (!gender) {
                                errorMessages.push("Hãy chọn giới tính.");
                            }

                            if (department === "") {
                                errorMessages.push("Hãy chọn phân khoa.");
                            }

                            var datePattern = /^\d{2}\/\d{2}\/\d{4}$/;
                            if (birthdate === "" || !datePattern.test(birthdate)) {
                                errorMessages.push("Hãy nhập ngày sinh đúng định dạng dd/mm/yyyy.");
                            } else {
                                var dateParts = birthdate.split('/');
                                var day = parseInt(dateParts[0], 10);
                                var month = parseInt(dateParts[1], 10);
                                var year = parseInt(dateParts[2], 10);

                                var isLeap = isLeapYear(year);

                                if (day < 1 || day > 31 || month < 1 || month > 12 || year < 0 || year > 2023 || (month === 2 && day > (isLeap ? 29 : 28))) {
                                    errorMessages.push("Hãy nhập ngày sinh hợp lệ.");
                                }
                            }

                            if (errorMessages.length > 0) {
                                var errorMessageHtml = "";
                                for (var i = 0; i < errorMessages.length; i++) {
                                    errorMessageHtml += errorMessages[i] + "<br>";
                                }
                                $("#errorMessages").html(errorMessageHtml);
                            } else {
                                // Nếu không có lỗi, thực hiện đăng ký hoặc xử lý form ở đây
                                $("#errorMessages").html(""); // Xóa thông báo lỗi cũ
                            }
                        });
                    });
                </script>
            </div>

            <div class="qna">
                <div class="name-box required-label" for="name">Họ và tên</div>
                <input class="name-input" type="text" id="name" name="name" required>
            </div>

            <!--  Giới tính -->
            <div class="qna">
                <div class="gender-box required-label" for="gender">Giới tính</div>
                <div class="gender-input" id="gender" name="gender">
                    <input type="radio" id="male" name="gender" value="Nam" required> Nam
                    <input type="radio" id="female" name="gender" value="Nữ" required> Nữ
                </div>
            </div>

            <div class="qna">
                <div class="department-box required-label" for="department">Phân khoa</div>
                <select class="department-select" id="department" name="department">
                    <option value="">--Chọn phân khoa--</option>
                    <?php
                    $departments = array('MAT' => 'Khoa học máy tính'
                                        ,'KDL' => 'Khoa học vật liệu');
                    foreach ($departments as $key => $value) {
                        echo "<option value=\"$key\">$value</option>";
                    }
                    ?>
                </select>
            </div>

            <div class="qna">
                <div class="birth-box required-label" for="birthdate">Ngày sinh</div>
                <input class="birth-input" type="text" id="birthdate" name="birthdate" placeholder="dd/mm/yyyy" required>
            </div>

            <div class="d-flex">
                <div class="address-box" for="address">Địa chỉ</div>
                <input class="address-input" type="text" id="address" name="address">
            </div>

            <div class="signup-button text-center" id="register-btn">
                <button class="btn signup" type="submit">Đăng ký</button>
            </div>
        </form>
    </div>
</body>
</html>
