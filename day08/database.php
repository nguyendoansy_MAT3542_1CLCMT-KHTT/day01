<?php
//  "các thông tin cần thiết" để thực hiện kết nối tới database
$dsn = 'mysql:host=localhost;dbname=ltweb;charset=utf8';
$username = 'root';
$password = '';
// khai báo $connect là biến toàn cục bằng cách thêm từ khóa global, để có thể sử dụng bên ngoài phạm vi database.php
global $connect;
try {
    // khởi tạo một đối tượng kết nối PDO tới CSDL MySQL
    $connect = new PDO($dsn, $username, $password);
    // Câu lệnh này thiết lập chế độ xử lý lỗi cho đối tượng PDO
    $connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
    // Với trường hợp lỗi khi thực hiện kết nối, thông tin lỗi sẽ được in ra
    echo 'Connection failed: '.$e->getMessage();
    die();
}




