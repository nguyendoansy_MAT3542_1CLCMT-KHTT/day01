<?php
// Kết nối CSDL
require 'database.php';
global $connect;

$departments = array('MAT' => 'Khoa học máy tính','KDL' => 'Khoa học vật liệu');
if ($_SERVER["REQUEST_METHOD"] == "GET"){
// Lấy giá trị department và keyword từ yêu cầu Ajax
$department = $_GET['department'];
$keyword = $_GET['keyword'];


// Xử lý truy vấn SQL và lấy HTML mới cho tbody
$sql;

if(!empty($department)) {
    $sql = "SELECT * FROM students WHERE students.department = '$department'";
}

if(!empty($keyword)) {
    $sql = "SELECT * FROM students WHERE  students.fullname LIKE '%$keyword%'";
}

if(!empty($department) && !empty($keyword)) {
    $sql = "SELECT * FROM students WHERE students.department = '$department' AND students.fullname LIKE '%$keyword%'";
}

if(empty($department) && empty($keyword)) {
    $sql = "SELECT * FROM students";
}

$statement = $connect->prepare($sql);
$statement->execute();
$newStudents = $statement->fetchAll();

// Tạo HTML mới cho tbody
$newTbodyHTML = '';
foreach ($newStudents as $student) {
  $newTbodyHTML .= '<tr>';
  $newTbodyHTML .= '<td>' . $student['id'] . '</td>';
  $newTbodyHTML .= '<td>' . $student['fullname'] . '</td>';
  $newTbodyHTML .= '<td>' . $departments[$student['department']] . '</td>';
  $newTbodyHTML .= '<td>';
  $newTbodyHTML .= '<button class="delete-button" onclick="openPopup()" data-id="' . $student['id'] . '">Xóa</button>';
  $newTbodyHTML .= '<button class="update-button" onclick="location.href=\'update_student.php?id=' . $student['id'] . '\'">Sửa</button>';
  $newTbodyHTML .= '</td>';
  $newTbodyHTML .= '</tr>';
}

// Trả về HTML mới cho tbody
echo $newTbodyHTML;
}

?>