<?php
require 'database.php';
global $connect;
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $id = $_POST['id'];
    $name = $_POST['name'];
    $gender = $_POST['gender'];
    $department = $_POST['department'];
    // $birthdate = $_POST['birthdate'];
    // $newBirthdate = date('Y/m/d', strtotime($birthdate));
    $date = DateTime::createFromFormat('d/m/Y', $_POST['birthdate']);
    $birthdate = $date->format('Y/m/d');


    $address = $_POST['address'];
    $image_path = "";
    // Tạo bản sao tại mục uploads
    if (isset($_FILES['profileImage']) && !empty($_FILES['profileImage']['name'])) {

        if ($_FILES['profileImage']['error'] == 0) {
            $target_dir = 'uploads/';        
            $target_file = $target_dir . basename($_FILES['profileImage']['name']);
            if(!file_exists($target_file)){
                if (move_uploaded_file($_FILES['profileImage']['tmp_name'], $target_file)) {
                } else {
                    exit('<script>alert("An error occurred while copying the file.");</script>');
                }
            } 
        }
        $image_path = $target_file;
    }
  

    // câu truy vấn SQL để sửa đổi dữ liệu (ta có từ form) vào bảng students

    $update = "UPDATE students
               SET
                   fullname = '$name',
                   gender = '$gender',
                   department = '$department',
                   birthdate = '$birthdate',
                   address = '$address'
               WHERE id = $id";
    
    if(!empty($image_path)){
        $update = "UPDATE students
               SET
                   fullname = '$name',
                   gender = '$gender',
                   department = '$department',
                   birthdate = '$birthdate',
                   address = '$address',
                   image = '$image_path'
               WHERE id = $id";
    }

    // // Thực thi truy vấn
    $statement = $connect -> prepare($update);
    $statement -> execute();

    // Chuyển hướng trở lại trang HTML
    header("Location: table.php");
    exit;
}
?>