<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./style.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <title>Chỉnh sửa thông tin</title>
</head>

<?php
// Kết nối CSDL
require 'database.php';
global $connect;
// Lấy sinh viên từ Table students
$id = $_GET['id'];
$sql = "SELECT * FROM students WHERE id = $id";
$statement = $connect -> prepare($sql);
$statement -> execute();
$student = $statement -> fetch();
$Date = date("d/m/Y", strtotime($student['birthdate']));
// Mảng các khoa
$departments = array('MAT' => 'Khoa học máy tính'
,'KDL' => 'Khoa học vật liệu');

?>

<body>
<div class="container">
    <!-- Form đăng ký với phương thức post tới confirm.php để hiển thị thông tin đăng ký -->
    <form id="registrationForm" class="reg-form" method="POST" action="update.php" enctype="multipart/form-data">

        <!-- ID trong table -->
        <input type="hidden" name="id" value="<?php echo $student['id']; ?>">

        <!-- Hiển thị lỗi trong việc điền đơn đăng ký -->
        <div id="errorMessages" class="error-messages">
            <script>
                $(document).ready(function () {
                    $("#register_btn").click(function(event) {
                        let name = $("#name").val();
                        let gender = $("input[name='gender']:checked").val();
                        let department = $("#department").val();
                        let birthdate = $("#birthdate").val();

                        // Biến flag để check xem có error messages về việc đăng không
                        let flag = true;

                        let errorMessages = [];

                        if (name === "") {
                            errorMessages.push("Hãy nhập tên.");
                            flag = false
                        }

                        if (!gender) {
                            errorMessages.push("Hãy chọn giới tính.");
                            flag = false
                        }

                        if (department === "") {
                            errorMessages.push("Hãy chọn phân khoa.");
                            flag = false
                        }

                        let datePattern = /^\d{2}\/\d{2}\/\d{4}$/;
                        if (birthdate === "" || !datePattern.test(birthdate)) {
                            errorMessages.push("Hãy nhập ngày sinh đúng định dạng dd/mm/yyyy.");
                            flag = false
                        }

                        if (!flag) {
                            // Với việc flag = flase tức là có thông tin điền vào không nhưng mong muốn, thì nút "đăng ký" sẽ không thực hiện action
                            event.preventDefault();
                            // Tổng hợp và hiện thông báo lỗi
                            let errorMessageHtml = "";
                            for (let i = 0; i < errorMessages.length; i++) {
                                errorMessageHtml += errorMessages[i] + "<br>";
                            }
                            $("#errorMessages").html(errorMessageHtml);
                        } else {
                            // Nếu không có lỗi, thực hiện đăng ký hoặc xử lý form ở đây
                            $("#errorMessages").html(""); // Xóa thông báo lỗi cũ
                        }
                    });
                });
            </script>
        </div>

        <!-- Mục Họ và tên -->
        <div class="form-group">
            <div class="full-name-box required-label"><label for="name">Họ và tên</label></div>
            <input class="full-name-input" type="text" id="name" name="name" value="<?php echo $student['fullname']; ?>" required>
        </div>

        <!-- Mục Giới tính -->
        <div class="form-group">
            <div class="gender-box required-label"><label for="gender">Giới tính</label></div>
            <div id="gender" class="gender-select">
                <input type="radio" id="male" name="gender"
                       value="Nam"
                        <?php if($student['gender'] == 'Nam'): ?>
                        checked
                        <?php endif; ?>
                       required><label for="male">Nam</label>
                <input type="radio" id="female" name="gender"
                       value="Nữ"
                        <?php if($student['gender'] == 'Nữ'): ?>
                        checked
                        <?php endif; ?>
                       required><label for="female">Nữ</label>
            </div>
        </div>

        <!-- Mục Phân khoa -->
        <div class="form-group">
            <div class="department-box required-label"><label for="department">Phân khoa</label></div>
            <select id="department" name="department" class="department-select">
                <?php
                foreach($departments as $key => $value) {
                    ?>
                    <option value="<?php echo $key; ?>"
                        <?php if($key == $student['department']): ?>
                            selected
                        <?php endif; ?>
                    >
                        <?php echo $value; ?>
                    </option>
                    <?php
                }
                ?>
            </select>
        </div>

        <!-- Mục Ngày sinh -->
        <div class="form-group">
            <div class="birth-box required-label"><label for="birthdate">Ngày sinh</label></div>
            <input class="birth-input" type="text" id="birthdate"
                   name="birthdate" required
                   value="<?php echo $Date;?>">
        </div>

        <!-- Mục địa chỉ -->
        <div class="form-group">
            <div class="address-box"><label for="address"></label>Địa chỉ</div>
            <input class="address-input" type="text" id="address"
                   name="address"
                   value="<?php echo $student['address']; ?>">
        </div>

        <!-- Mục Hình ảnh -->

  <div class="form-group" style="align-items: unset">
  <div class="image-box" style="height: 20%"><label for="image">Hình ảnh</label></div>
  <div class="show-image">
    <?php if (!empty($student['image'])): ?>
        <img src="<?php echo $student['image']; ?>" style="width:100%">
    <?php else: ?>
        <!-- Hình ảnh mặc định hoặc hình ảnh trống -->
        <img src="./uploads/default.png" style="width:100%">
    <?php endif; ?>
</div>

  <div class = "image-input">
  <input type="file" id="image" name="profileImage" accept="image/*" style="display: none;">
  </div>

    <script>
    const currentImg = document.querySelector('.show-image img');
    const fileInput = document.getElementById('image');

    currentImg.addEventListener('click', () => {
    fileInput.click();
    });

    fileInput.addEventListener('change', () => {
    const file = fileInput.files[0];
    const reader = new FileReader();

    const maxFileSize = 1000000; // 1000KB
    const allowedExtensions = ['jpg', 'jpeg', 'png', 'gif'];
    const imageFileType = file.name.split('.').pop().toLowerCase();

    if (file.size > maxFileSize) {
    alert('Kích thước của ảnh quá lớn');
    return;
    }

    if (!allowedExtensions.includes(imageFileType)) {
    alert('Sai định dạng của ảnh');
    return;
    }

    reader.onload = function(e) {
    currentImg.src = e.target.result;
    };

    reader.readAsDataURL(file);
    });
    </script>
    </div>


        <!-- Nút Đăng ký -->
        <div class="signup-button" id="register_btn">
            <button type="submit">Xác nhận</button>
        </div>
    </form>
</div>
</body>

</html>