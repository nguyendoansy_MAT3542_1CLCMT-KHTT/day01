<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <link rel="stylesheet" href="./tableStyle.css">
    <title>Danh sách sinh viên</title>
</head>
<!--Thiết lập kết nối và lấy danh sách students từ database -->
<?php
// Kết nối CSDL
require 'database.php';
global $connect;
// Lấy danh sách sinh viên từ Table students
$sql = "SELECT * FROM students";
$statement = $connect -> prepare($sql);
$statement -> execute();
$students = $statement -> fetchAll();

// Mảng các khoa
$departments = array('MAT' => 'Khoa học máy tính'
                    ,'KDL' => 'Khoa học vật liệu');

?>
<body>
<div class="container">
    <div class="row">
        <!-- Row 1 - Search form -->
        <div class="col-12">
            <div class="search-form">
                <form>
                    <div class="form-group">
                        <div class="department"><label for="department">Khoa</label></div>
                        <select id="department" name="department" class="department-select">
                            <option value=""></option>
                            <?php
                            foreach ($departments as $key => $value) {
                                echo "<option value=\"$key\">$value</option>";
                            }
                            ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <div class="keyword"><label for="keyword">Từ khoá</label></div>
                        <input class="keyword-input" type="text" id="keyword" name="keyword">
                    </div>
                    <div class="search-button">
                        <button type="submit">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row">

        <!-- Row 2 - Add student button -->
        <div class="col-12">
            <div class="add-student">
                <!-- code for add student -->
                <div class="page-title">Số sinh viên tìm thấy: xxx </div>
                <div class="add-button">
                    <button onclick="location.href='register.php'">Thêm</button>
                </div>
            </div>
        </div>
    </div>

    <div class="row">

        <!-- Row 3 - Student table -->
        <div class="col-12">

            <table class="table" style="width: 800px">
                <thead style="text-align: left;">
                <tr>
                    <th width="10%">No</th>
                    <th width="35%">Họ tên</th>
                    <th width="35%">Khoa</th>
                    <th width="20%">Action</th>
                </tr>
                </thead>

                <tbody id="tbody">
  <?php foreach ($students as $student): ?>
    <tr>
      <td><?php echo $student['id']; ?></td>
      <td><?php echo $student['fullname']; ?></td>
      <td><?php echo $departments[$student['department']]; ?></td>
      <td>
        <button class="delete-button" onclick="openPopup()" data-id="<?= $student['id'] ?>">Xóa</button>
        <button class="update-button" onclick="location.href='update_student.php?id=<?php echo $student['id'] ?>'">Sửa</button>
      </td>
    </tr>
  <?php endforeach; ?>
</tbody>
            </table>
        </div>
    </div>

    <!-- Popup -->
    <div id="popup" style="display:none">
        <div class="popup-content">
            <p>Bạn muốn xóa sinh viên này?</p>
            <button id="cancel" class="action-button" onclick="closePopup()">Hủy</button>
            <button id="delete" class="action-button">Xóa</button>
        </div>
    </div>

</div>
</body>
<!-- Popup Script and Style -->
<script>
    // Open popup function
    function openPopup() {
        let btn = event.target;
        let studentId = btn.dataset.id;
        document.getElementById("popup").style.display = "block";
        // Xoá sinh viên với ID
        $('#delete').click(function(){

            $.ajax({
                url: 'delete-student.php',
                type: 'POST',
                data: {id: studentId}
            })
                .done(function(response){
                        closePopup();
                        // Load lại trang sau 1 giây
                        setTimeout(function(){
                            location.reload();
                        
                        }, 1000);

                })

        })
    }
    // Close popup function
    function closePopup() {
        document.getElementById("popup").style.display = "none";
    }
</script>
<style>
    /* Popup styles */
    #popup {
        position: fixed;
        top: 50%;
        left: 50%;
        width: 400px;
        height: 150px;
        transform: translate(-50%, -50%);
        background-color: white;
        padding: 20px;
        border: 2px solid rgb(5 84 149);
    }

    #popup .popup-content button {
        float: right;
        padding: 12px 5px;
        margin-left: 5px;
    }

</style>

<script>
// Gửi yêu cầu Ajax khi department thay đổi
document.getElementById('department').addEventListener('change', updateTableBody);

// Gửi yêu cầu Ajax khi keyword thay đổi
document.getElementById('keyword').addEventListener('input', updateTableBody);

function updateTableBody() {
  var department = document.getElementById('department').value;
  var keyword = document.getElementById('keyword').value;

  // Gửi yêu cầu Ajax để cập nhật nội dung của tbody
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
    if (xhr.readyState === 4 && xhr.status === 200) {
      var newTbodyHTML = xhr.responseText;
      document.getElementById('tbody').innerHTML = newTbodyHTML;
    }
  };
  var url = 'update_table_body.php?department=' + encodeURIComponent(department) + '&keyword=' + encodeURIComponent(keyword);
  xhr.open('GET', url, true);
  xhr.send();
}
</script>


</html>