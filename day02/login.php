<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./style.css">
    <title>Document</title>
</head>

<body>
    <form action="" class="bd-blue">
    <?php
			// Phần thông tin về thứ, giờ, ngày, ... 
            date_default_timezone_set('Asia/Ho_Chi_Minh'); // Thiết lập múi giờ Việt Nam
            $today = date("H:i:s"); // Lấy ra thông tin giờ: phút: giây của hiện tại
            $dayOfWeek = date("N"); // Lấy ra thứ trong tuần (1 - Thứ 2, 2 - Thứ 3, ..., 7 - Chủ Nhật)
            $date = date("d/m/Y"); // Lấy ra ngày/ tháng/ năm của hiện tại
            
            // Mảng ánh từ tiếng Anh sang tiếng Việt
            $daysInVietnamese = [
                1 => 'Thứ 2',
                2 => 'Thứ 3',
                3 => 'Thứ 4',
                4 => 'Thứ 5',
                5 => 'Thứ 6',
                6 => 'Thứ 7',
                7 => 'Chủ Nhật',
            ];

            $dayOfWeekVietnamese = $daysInVietnamese[$dayOfWeek]; // Lấy ra tên thứ trong tiếng Việt
            
            ?>
            <p class="present-time p-8">Bây giờ là
                <?php echo $today; ?>,
                <?php echo $dayOfWeekVietnamese; ?> ngày
                <?php echo $date; ?>
            </p>
			
		<!-- Phần Login -->
        <div class="d-flex form-input mb-20">
            <div class="w-50 p-20 bg-blue text-white me-20 bd-blue">Tên đăng nhập</div>
            <div class="w-50 ">
                <input class="w-100 bd-blue" type="text">
            </div>
        </div>
        <div class="d-flex form-input pass-input">
            <div class="w-50 p-20 bg-blue text-white me-20 bd-blue">Mật khẩu</div>
            <div class="w-50 ">
                <input class="w-100 bd-blue" type="password">
            </div>
        </div>
        <div class="text-center ">
            <button type="submit" class="btn bd-blue bg-blue text-white">Đăng nhập</button>
        </div>
    </form>
</body>

</html>