<?php
//  "các thông tin cần thiết" để thực hiện kết nối tới database
$dsn = 'mysql:host=localhost;dbname=ltweb;charset=utf8';
$username = 'root';
$password = '';

try {
    // khởi tạo một đối tượng kết nối PDO tới CSDL MySQL
    $connect = new PDO($dsn, $username, $password);
    // Câu lệnh này thiết lập chế độ xử lý lỗi cho đối tượng PDO
    $connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Ta gọi hàm insert để thực hiện lưu dữ liệu từ form vào database
    insertData($connect);
    // Sau khi thực hiện insertData, ta in ra chuỗi thông báo "thành công" này
    echo 'Inserted information into Database';
} catch(PDOException $e) {
    // Với trường hợp lỗi khi thực hiện kết nối, thông tin lỗi sẽ được in ra
    echo 'Connection failed: '.$e->getMessage();
    die();
}

// Ta xây dựng một hàm thực hiện insertData vào trong Database
function insertData(PDO $connect)
{
    if ($_SERVER["REQUEST_METHOD"] == "POST") {

        $name = $_POST['name'];
        $gender = $_POST['gender'];
        $department = $_POST['department'];
        $birthdate = $_POST['birthdate'];
        $address = $_POST['address'];
        $path_image = $_POST['image'];

        // câu truy vấn SQL để chèn dữ liệu (ta có từ form) vào bảng students
        $sql = "INSERT INTO students (fullname, gender,department,birthdate, address, image) 
            VALUES ('$name','$gender' ,'$department','$birthdate', '$address', '$path_image')";
        // Thực thi truy vấn
        $connect->exec($sql);
    }
}


