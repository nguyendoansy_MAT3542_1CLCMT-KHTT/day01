-- Tạo database ltweb
CREATE DATABASE IF NOT EXISTS ltweb;

-- Tạo bảng students với các thông tin ràng buộc từ form đăng ký 
CREATE TABLE IF NOT EXISTS students (
    id INT AUTO_INCREMENT PRIMARY KEY,
    fullname VARCHAR(100) NOT NULL, 
    gender VARCHAR(5) NOT NULL,
    department VARCHAR(100) NOT NULL,
    birthdate  DATE NOT NULL,
    address TEXT,
    image VARCHAR(255)
);