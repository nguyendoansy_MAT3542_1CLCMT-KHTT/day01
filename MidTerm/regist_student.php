<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./style.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <title>Hiển thị thông tin sinh viên</title>
</head>


<body>
<div class="container">
    <!-- Khi đã xác nhận lại thông tin ta sẽ thực hiện connect và insert data vào Database ở bên database.php -->
    <h1>Form đăng ký sinh viên</h1>
    <form id="registrationForm" class="reg-form">
        <!-- Hiển thị Họ và tên trong form đăng ký  -->
        <div class="form-group">
            <div class="full-name-box required-label"><label for="name">Họ và tên</label></div>
            <div class="show-name">
                <?php
                echo $_POST['name'];
                ?>
            </div>
            <!-- trường input này là lưu giữ giá trị người dùng nhập cho trường "Họ và tên" khi gửi form, dùng để gửi qua các trang tiếp theo mà không hiển thị-->
            <input type = "hidden" name = "name" value = "<?php echo $_POST['name'];?>">
        </div>

        <!-- Hiển thị Giới tính trong form đăng ký  -->
        <div class="form-group">
            <div class="gender-box required-label"><label for="gender">Giới tính</label></div>
            <div class="show-gender">
                <?php
                echo $_POST['gender'];
                ?>
            </div>
            <input type = "hidden" name = "gender" value = "<?php echo $_POST['gender'];?>">
        </div>


        <!-- Hiển thị Ngày sinh trong form đăng ký  -->
        <div class="form-group">
            <div class="birth-box required-label"><label for="birthdate">Ngày sinh</label></div>
            <div class="show-birthdate">
                <?php
                echo $_POST['birthdate'];
                ?>
            </div>
            <input type = "hidden" name = "birthdate" value = "<?php echo $_POST['birthdate'];?>">
        </div>

        <!-- Hiển thị Địa chỉ (nếu có)  -->
        <div class="form-group">
            <div class="address-box"><label for="address"></label>Địa chỉ</div>
            <div class="show-address">
                <?php
                echo $_POST['address'];
                ?>
            </div>
            <input type = "hidden" name = "address" value = "<?php echo $_POST['address'];?>">
        </div>

        <div class="form-group">
            <div class="infor-box"><label for="infor"></label>Thông tin khác</div>
            <div class="show-infor">
                <?php
                echo $_POST['infor'];
                ?>
            </div>
            <input type = "hidden" name = "infor" value = "<?php echo $_POST['infor'];?>">
        </div>
    </form>
</div>
</body>

</html>