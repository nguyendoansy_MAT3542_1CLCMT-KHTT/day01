<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./style.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <title>Form đăng ký sinh viên</title>
</head>

<body>
<div class="container">
    <!-- Form đăng ký với phương thức post tới confirm.php để hiển thị thông tin đăng ký -->
    <form id="registrationForm" class="reg-form" method="POST" action="regist_student.php" enctype="multipart/form-data">
        <!-- Hiển thị lỗi trong việc điền đơn đăng ký -->
        <div id="errorMessages" class="error-messages">
            <script>
                $(document).ready(function () {
                    $("#register_btn").click(function(event) {
                        let name = $("#name").val();
                        let gender = $("input[name='gender']:checked").val();

                        // Biến flag để check xem có error messages về việc đăng không
                        let flag = true;

                        let errorMessages = [];

                        if (name === "") {
                            errorMessages.push("Hãy nhập tên.");
                            flag = false
                        }

                        if (!gender) {
                            errorMessages.push("Hãy chọn giới tính.");
                            flag = false
                        }


                        if (!flag) {
                            // Với việc flag = flase tức là có thông tin điền vào không nhưng mong muốn, thì nút "đăng ký" sẽ không thực hiện action
                            event.preventDefault();
                            // Tổng hợp và hiện thông báo lỗi
                            let errorMessageHtml = "";
                            for (let i = 0; i < errorMessages.length; i++) {
                                errorMessageHtml += errorMessages[i] + "<br>";
                            }
                            $("#errorMessages").html(errorMessageHtml);
                        } else {
                            // Nếu không có lỗi, thực hiện đăng ký hoặc xử lý form ở đây
                            $("#errorMessages").html(""); // Xóa thông báo lỗi cũ
                        }
                    });
                });
            </script>
        </div>
        <!-- Mục Họ và tên -->
        <div class="form-group">
            <div class="full-name-box required-label"><label for="name">Họ và tên</label></div>
            <input class="full-name-input" type="text" id="name" name="name">
        </div>
        <!-- Mục Giới tính -->
        <div class="form-group">
            <div class="gender-box required-label"><label for="gender">Giới tính</label></div>
            <div id="gender" class="gender-select">
                <input type="radio" id="male" name="gender" value="Nam"  ><label for="male">Nam</label>
                <input type="radio" id="female" name="gender" value="Nữ" ><label for="female">Nữ</label>
            </div>
        </div>

        <!-- Mục Ngày sinh -->
        <div class="form-group">
            <div class="birth-box required-label"><label for="birthdate">Ngày sinh</label></div>
            <label class="col-sm-2" for="year">Năm</label>
            <select class="department-select form-control col-sm-10" name="year">
                <option value="" selected disabled></option>
                <?php
                $year_current = date("Y");
                for ($i = $year_current - 40; $i <= $year_current - 15; $i++) {
                    echo "<option value='$i'>$i</option>";
                }
                ?>
            </select>

            <label class="col-sm-2" for="month">Tháng</label>
            <select class="department-select form-control col-sm-10" name="month">
                <option value="" selected disabled></option>
                <?php
                for ($i = 1; $i <= 12; $i++) {
                    echo "<option value='$i'>$i</option>";
                }
                ?>
            </select>

            <label class="col-sm-2" for="year">Ngày</label>
            <select class="department-select form-control col-sm-10" name="day">
                <option value="" selected disabled></option>
                <?php
                for ($i = 1; $i <= 31; $i++) {
                    echo "<option value='$i'>$i</option>";
                }
                ?>
            </select>
        </div>

        <!-- Mục Địa chỉ -->
        <div class="form-group">
            <div class="department-box required-label"><label for="city">Địa chỉ</label></div>
            <label class="col-sm-2" for="year">Thành phố</label>
            <select id="city" name="city" class="department-select" required style="display: inline-block;">
                <option value="">--Chọn thành phố--</option>
                <?php
                $cities = array('HN' => 'Hà Nội', 'HCM' => 'Thành phố Hồ Chí Minh');
                foreach ($cities as $key => $value) {
                    echo "<option value=\"$key\">$value</option>";
                }
                ?>
            </select>

<!--            <div class="department-box required-label"><label for="district"></label></div>-->
            <label class="col-sm-2" for="year">Quận</label>
            <select id="district" name="district" class="department-select" style="display: inline-block;">
                <option value="">--Chọn quận--</option>
            </select>
        </div>

        <!-- Mục địa chỉ -->
        <div class="form-group">
            <div class="infor-box"><label for="infor"></label>Thông tin khác</div>
            <textarea class="infor-input" id="infor" name="infor"></textarea>
        </div>

        <!-- Nút Đăng ký -->
        <div class="signup-button" id="register_btn">
            <button type="submit">Đăng ký</button>
        </div>
    </form>
</div>
</body>



</html>