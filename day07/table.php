<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./tableStyle.css">
    <title>Danh sách sinh viên</title>
</head>
<!--Thiết lập kết nối và lấy danh sách students từ database -->
<?php
// Kết nối CSDL
require 'database.php';
global $connect;
// Lấy danh sách sinh viên từ Table students
$sql = "SELECT * FROM students";
$statement = $connect -> prepare($sql);
$statement -> execute();
$students = $statement -> fetchAll();
$count = count($students);

// Mảng các khoa
$departments = array('MAT' => 'Khoa học máy tính'
                    ,'KDL' => 'Khoa học vật liệu');
?>
<body>

<div class="container">
    <div class="row">
        <!-- Row 1 - Search form -->
        <div class="col-12">
            <div class="search-form">
                <form>
                    <div class="form-group">
                        <div class="department"><label for="department">Khoa</label></div>
                        <select id="department" name="department" class="department-select">
                            <option value=""></option>
                            <?php
                            foreach ($departments as $key => $value) {
                                echo "<option value=\"$key\">$value</option>";
                            }
                            ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <div class="keyword"><label for="keyword">Từ khoá</label></div>
                        <input class="keyword-input" type="text" id="keyword" name="keyword">
                    </div>
                    <div class="search-button">
                        <button type="submit">Tìm kiếm</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row">

        <!-- Row 2 - Add student button -->
        <div class="col-12">
            <div class="add-student">
                <!-- code for add student -->
                <div class="page-title">Số sinh viên tìm thấy: <?php echo $count; ?></div>
                <div class="add-button">
                    <button onclick="location.href='register.php'">Thêm</button>
                </div>
            </div>
        </div>

    </div>

    <div class="row">

        <!-- Row 3 - Student table -->
        <div class="col-12">

            <table class="table" style="width: 800px">
                <thead style="text-align: left;">
                <tr>
                    <th width="10%">No</th>
                    <th width="35%">Họ tên</th>
                    <th width="35%">Khoa</th>
                    <th width="20%">Action</th>
                </tr>
                </thead>

                <tbody>
                <?php
                $number = 1;
                foreach ($students as $student): ?>
                    <tr>
                        <td><?php echo $number; ?></td>
                        <td><?php echo $student['fullname']; ?></td>
                        <td><?php echo $departments[$student['department']]; ?></td>
                        <td>
                            <button class="action-button">Xóa</button>
                            <button class="action-button">Sửa</button>
                        </td>
                    </tr>
                <?php
                $number++;
                endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

</body>
</html>