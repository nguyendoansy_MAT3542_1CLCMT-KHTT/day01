<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./style.css">
    <title>Đăng ký Tân sinh viên</title>
</head>

<body>
        <form action="" class="bd-blue">
            <table class="w-100">
                <!-- Name -->
                <tr>
                    <td class="name-box"><label for="full-name">Họ và tên</label></td>
                    <td class="name-input">
                        <input type="text" id="full-name" name="full-name" required class="name-input-required">
                    </td>
                </tr>

                <!-- Gender -->
                <tr>
                    <td class="gender-box"><label for="gender">Giới tính</label></td>
                    <td>
                        <div class="d-flex">
                            <?php
                            $values  = array(0 => 'Nam', 1 => 'Nữ');
                            $genders = array(0 => 'Male', 1 => 'Female');
                            for($i = 0; $i < count($genders); $i++) {
                                 $key = $genders[$i];
                                 $value = $values[$i];

                            echo "<div class='genders'>
                            <input id='$key' type='radio' name='genders' value='$value'>
                            <label for='$key'>$value</label></div>";
                            }
                            ?>
                        </div>
                    </td>
                </tr>

                <!-- Department -->
                <tr>
                    <td class="department-box"><label for="department">Phân khoa</label></td>
                    <td>
                        <select id="department" name="department" class="department-select">
                            <option value>--Chọn phân khoa--</option>
                            <?php
                            $departments = array('MAT' => 'Khoa học máy tính'
                                                ,'KDL' => 'Khoa học vật liệu');
                            foreach($departments as $key => $value) {
                            echo "<option value=\"$key\">$value</option>";
                            }
                            ?>
                        </select>
                    </td>
                </tr>
            </table>

            <!-- Button Signup -->
            <div class="text-center ">
                <button type="submit" class="btn signup">Đăng ký</button>
            </div>
        </form>
    </body>
</html>